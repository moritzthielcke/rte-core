package io.rte.core.sq.tile.level.loader.tiled;

import io.rte.core.sq.tile.*;
import io.rte.core.sq.tile.level.TiledLayer;
import io.rte.core.sq.tile.level.TiledLevel;
import io.vertx.core.Promise;
import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description: Load a maps from the tiled-editor
 */
public class TiledMapLoader {
    private static final Logger log = LoggerFactory.getLogger(TiledMapLoader.class);
    private final String directory;
    private final FileSystem fs;
    private final AbstractTileGame game;

    /***
     *
     * @param fs your filesystem
     * @param directory the directory containing the map assets
     */
    public TiledMapLoader(String directory, FileSystem fs, AbstractTileGame game) {
        this.directory = directory;
        this.fs = fs;
        this.game = game;
    }

    /***
     * loads a map from a .json file
     * @param fileName the file to load
     * @return the promised map
     */
    public Promise<TiledLevel> loadMapFromJsonFile(String levelName, String fileName) {
        Promise<TiledLevel> promise = Promise.promise();
        fs.readFile(directory + "/" + fileName, readLevel -> {
            if (readLevel.succeeded()) {
                JsonObject rawMap = new JsonObject(readLevel.result());
                try {
                    TiledLevel level = loadLevel(levelName, rawMap);
                    //load tile sets:
                    JsonArray tileSets = rawMap.getJsonArray("tilesets");
                    for (int i = 0; i < tileSets.size(); i++) {
                        JsonObject tileset = tileSets.getJsonObject(i);
                        final int num = i + 1;
                        fs.readFile(directory + "/" + tileset.getString("source"), tilesetResult -> {
                            if (tilesetResult.succeeded()) {
                                JsonObject tilesetDetails = new JsonObject(tilesetResult.result());
                                addTileTypes(level, tileset, tilesetDetails);
                                //everything loaded?
                                if (num >= tileset.size()) {
                                    initialize(level);
                                    promise.complete(level);
                                }
                            } else {
                                promise.fail(tilesetResult.cause());
                            }
                        });
                    }
                } catch (UnsupportedCompressionException | InvalidMapFormatException e) {
                    promise.fail(e);
                }
            } else {
                promise.fail(readLevel.cause());
            }
        });
        return promise;
    }


    /***
     * initializes the level (builds layer). do it , after all types are set
     * @param level the level to initialize
     */
    private void initialize(TiledLevel level) {
        log.info("initializing {}", level);
        for (JsonObject in : level.getInputLayer()) {
            List<List<Tile>> tileset = new ArrayList<>(level.getHeight());
            JsonArray data = in.getJsonArray("data");
            //build the layer right-down:
            for (int i = 0; i < level.getWidth(); i++) {
                tileset.add(new ArrayList<>(level.getWidth()));
            }
            int x = 0, y = 0;
            for (int i = 0; i < data.size(); i++) {
                if (x >= level.getWidth()) {
                    y++;
                    x = 0;
                }
                Tile tile = null;
                TileType type = level.getTileTypes().get(data.getInteger(i));
                if (data.getInteger(i) != 0 && type == null) { //create empty tile
                    log.warn("cant find type for " + data.getInteger(i));
                } else if (type != null) {
                    try {
                        tile = new Tile(x, y, type.isWalkAble(), type.getCost());
                        tile.setProperties(type.getProperties());
                        tile.setType(type);
                        if(type.getSpecification()!=null){
                            type.getSpecification().onCreate(tile);
                        }
                    } catch (Exception ex) {
                        log.warn("error creating tile", ex);
                    }
                }
                if (tile == null) {
                    tile = new Tile(x, y, true, 0);
                }
                tileset.get(x).add(tile);
                x++;
            }
            //persist to array and save the layer
            Tile[][] array = new Tile[tileset.size()][];
            for (int i = 0; i < tileset.size(); i++) {
                array[i] = tileset.get(i).toArray(new Tile[0]);
            }
            log.info("created layer {} : {} * {}", in.getString("name"), tileset.size(), tileset.get(0).size());
            level.getLayer().put(in.getInteger("id"), new TiledLayer(in.getInteger("id"), in.getString("name"), in.getString("type"), new TileSet<>(array)));
        }
        //aggregate layer
        log.info("building aggregation layer...");
        level.getLayer().values().forEach(layer -> {
            Tile[][] tiles = layer.getTileSet().getTiles();
            if (level.getAggregationLayer() == null) {
                Tile[][] aggregation = new Tile[tiles.length][tiles[0].length];
                for (int x = 0; x < tiles.length; x++) {
                    for (int y = 0; y < tiles[x].length; y++) {
                        Tile src = layer.getTileSet().getTile(x, y);
                        aggregation[x][y] = new Tile(src.getX(), src.getY(), src.isWalkAble(), 0, src.isAccessAble());
                    }
                }
                TiledLayer aggregationLayer = new TiledLayer(-1, "aggregation", "aggregation", new TileSet(aggregation));
                level.setAggregationLayer(aggregationLayer);
            }
            for (int x = 0; x < tiles.length; x++) {
                for (int y = 0; y < tiles[x].length; y++) {
                    level.aggregate(level.getAggregationLayer().getTileSet().getTile(x, y), layer.getTileSet().getTile(x, y));
                }
            }
        });
        log.info("{} is ready! ", level.getName());
    }


    /***
     * creates a base level item without tile types
     *
     * @param src map json
     * @return the created map
     * @throws UnsupportedCompressionException compressed map
     * @throws InvalidMapFormatException load error
     */
    private TiledLevel loadLevel(String name, JsonObject src) throws UnsupportedCompressionException, InvalidMapFormatException {
        int compressionLevel = src.getInteger("compressionlevel");
        if (compressionLevel != -1) {
            throw new UnsupportedCompressionException("compressionlevel " + compressionLevel + ". Currently only -1 (CSV) is supported");
        }
        if (!"right-down".equals(src.getString("renderorder"))) {
            throw new InvalidMapFormatException("invalid render order (needs to be right-down)");
        }
        List<JsonObject> layers = new ArrayList<>();
        JsonArray layerArray = src.getJsonArray("layers");
        for (int i = 0; i < layerArray.size(); i++) {
            layers.add(layerArray.getJsonObject(i));
        }
        return new TiledLevel(name, src.getInteger("height"), src.getInteger("width"), layers);
    }


    /***
     * adds tile types to the map
     * @param level the map to add tiles to
     * @param tilesetInfo the tilesetinfo
     * @param tileset the tileset itself
     */
    private void addTileTypes(TiledLevel level, JsonObject tilesetInfo, JsonObject tileset) {
        JsonArray tiles = tileset.getJsonArray("tiles");
        for (int i = 0; i < tiles.size(); i++) {
            JsonObject rawType = tiles.getJsonObject(i);
            JsonObject properties = new JsonObject();
            boolean walkable = true;
            int cost = 0;
            int id = rawType.getInteger("id") + tilesetInfo.getInteger("firstgid");
            for (int p = 0; p < rawType.getJsonArray("properties").size(); p++) {
                JsonObject property = rawType.getJsonArray("properties").getJsonObject(p);
                if ("walkable".equals(property.getString("name"))) {
                    walkable = property.getBoolean("value", true);
                } else if ("cost".equals(property.getString("name"))) {
                    cost = property.getInteger("value", 0);
                } else {
                    properties.put(property.getString("name"), new JsonObject().put("type", property.getString("type")).put("value", property.getValue("value")));
                }
            }
            String fileName = new File(tilesetInfo.getString("source")).getName();
            TileType type = new TileType(id,
                    fileName,
                    rawType.getInteger("id"),
                    walkable, cost, properties
            );
            //add specification
            if (game.getTileSpecification() != null) {
                TileSpecification spec = game.getTileSpecification().get(fileName+ ":" + rawType.getInteger("id"));
                if (spec != null) {
                    type.setSpecification(spec);
                    spec.getProperties().fieldNames().forEach(prop -> {
                        //inherit property values
                        if (properties.getValue(prop) == null) {
                            properties.put(prop, spec.getProperties().getValue(prop));
                        }
                    });
                }
            }
            level.addTileType(id, type);
            log.info("tile type: {}", type);
        }
    }

}
