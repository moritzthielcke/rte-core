package io.rte.core.sq.tile.pathfinder;

import io.rte.core.algo.astar.Scorer;
import io.rte.core.sq.tile.Tile;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description: Scores the movement (currently only in avarage mode)
 */
public class TileScorer implements Scorer<Tile> {
    @Override
    public double computeCost(Tile from, Tile to) {
        //@todo this has to be the same as in the game logic (turn cost)
        return (int) Math.ceil((double) from.getCost()+to.getCost() / 2);
        //return Math.max(from.getCost(), to.getCost());
    }
}
