package io.rte.core.sq.tile.level.loader.tiled;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description:
 */
public class InvalidMapFormatException extends Exception{
    public InvalidMapFormatException(String message) {
        super(message);
    }
}
