package io.rte.core.sq.tile.pathfinder;

import io.rte.core.algo.astar.Graph;
import io.rte.core.sq.tile.Tile;
import io.rte.core.sq.tile.TileSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description: Tile Navigation Impl.
 */
public class TileNavigation<T extends Tile> {
    private static final Logger log = LoggerFactory.getLogger(TileNavigation.class);
    public enum Directions{
        UP, DOWN, RIGHT, LEFT, RIGHT_UP, RIGHT_DOWN, LEFT_UP, LEFT_DOWN
    }
    private final TileSet<T> tileSet;
    private final Graph<T> graph;

    public TileNavigation(TileSet<T> tileSet){
        this.tileSet = tileSet;
        log.info("creating new navigation graph for tile-set -> {}",  tileSet);
        Set<T> nodes = new HashSet<>(tileSet.getLengthX() * tileSet.getLengthY(0));
        Map<String, Set<T>> connectionMap = new HashMap<>();
        for(int x=0; x < tileSet.getLengthX(); x++){
            for(int y=0; y < tileSet.getLengthY(x);y++){
                T t = tileSet.getTile(x,y);
                nodes.add(t);
                log.debug("checking connections for -> {}", t.getId());
                Set<T> connections = getWaypoints(t);
                log.debug("connections => {}", connections);
                connectionMap.put(t.getId(), connections);
            }
        }
        this.graph = new Graph<>(nodes, connectionMap);
        log.info("graph created ({})", this.graph);
    }


    /***
     * gets connected and walkable waypoints from tile t
     * @param tile source tile
     * @return Set of access able waypoints
     */
    public Set<T> getWaypoints(T tile){
        Set<T> result = new HashSet<>();
        //@todo check for wall corners (see code for arrows line of sight)
       Arrays.stream(Directions.values()).forEach( direction -> {
           int targetX=tile.getX();
           int targetY=tile.getY();
            switch (direction){
                case UP -> targetY = tile.getY()-1;
                case DOWN -> targetY = tile.getY()+1;
                case LEFT -> targetX = tile.getX()-1;
                case RIGHT -> targetX = tile.getX()+1;
                case RIGHT_UP -> {
                    targetX = tile.getX()+1;
                    targetY = tile.getY()-1;
                }
                case RIGHT_DOWN -> {
                    targetX = tile.getX()+1;
                    targetY = tile.getY()+1;
                }
                case LEFT_UP -> {
                    targetX = tile.getX()-1;
                    targetY = tile.getY()-1;
                }
                case LEFT_DOWN -> {
                    targetX = tile.getX()-1;
                    targetY = tile.getY()+1;
                }
            }
            //does the target exists?
            if(targetX >= 0 && targetY >= 0 && (targetX < tileSet.getLengthX()) && (targetY < tileSet.getLengthY(targetX))){
                //is the target accessible ?
                T t = tileSet.getTile(targetX,targetY);
                log.debug("taking a look at tile -> {} ({})", t, direction);
                if( t != null && tile.isAccessAble() ){
                    result.add(t);
                }
            }
       });
       return result;
    }

    public TileSet<T> getTileSet() {
        return tileSet;
    }

    public Graph<T> getGraph() {
        return graph;
    }
}
