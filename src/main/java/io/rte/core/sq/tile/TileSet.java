package io.rte.core.sq.tile;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description: a basic tileset
 *
 */
public class TileSet<T extends Tile> {
    private final T[][] tileset;

    public TileSet(T[][] tileset) {
        this.tileset = tileset;
    }

    public T getTile(int x, int y){
        return tileset[x][y];
    }

    public T[][] getTiles() {
        return tileset;
    }

    public int getLengthX(){
        return tileset.length;
    }

    public int getLengthY(int pos){
        return tileset[pos].length;
    }

    @Override
    public String toString() {
        String origin = super.toString();
        return "TileSet{" +
                "ref='" + origin+ "'," +
                "size='" + getLengthX() + "/" + getLengthY(0) + "'"+
                '}';
    }
}
