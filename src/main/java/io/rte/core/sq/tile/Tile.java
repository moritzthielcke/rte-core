package io.rte.core.sq.tile;

import io.rte.core.algo.astar.GraphNode;
import io.vertx.core.json.JsonObject;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description: A base "Tile" entity
 */
public class Tile implements GraphNode {
    private final int x;
    private final int y;
    private boolean walkAble;
    private boolean accessAble;
    private int cost; //turn cost
    private JsonObject properties;
    private TileType type;

    public Tile(int x, int y) {
        this(x,y,true);
    }

    public Tile(int x, int y, boolean walkAble) {
        this(x, y, walkAble, 1);
    }

    public Tile(int x, int y, boolean walkAble, int cost){
        this(x, y, walkAble, cost, true);
    }

    public Tile(int x, int y, boolean walkAble, int cost, boolean accessAble) {
        this.x = x;
        this.y = y;
        this.walkAble = walkAble;
        this.cost = cost;
        this.accessAble = accessAble;
    }

    public TileType getType() {
        return type;
    }

    public void setType(TileType type) {
        this.type = type;
    }

    public JsonObject getProperties() {
        return properties;
    }

    public void setProperties(JsonObject properties) {
        this.properties = properties;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public boolean isWalkAble() {
        return walkAble;
    }

    public void setWalkAble(boolean walkAble) {
        this.walkAble = walkAble;
    }

    public void setAccessAble(boolean accessAble) {
        this.accessAble = accessAble;
    }

    /***
     *
     * @return  is it walkable and access-able?
     */
    public boolean isAccessAble(){
        return this.walkAble && this.accessAble;
    }

    @Override
    public String getId() {
        String id = super.toString();
        return id+"={x="+this.x+",y="+this.y+"}";
    }
    

    @Override
    public String toString() {
        String rs = "Tile{" +
                "x=" + x +
                ", y=" + y +
                ", walkAble=" + walkAble +
                ", accessAble=" + isAccessAble() +
                ", cost=" + cost +
                ", ref=" + super.toString();
        if(properties!=null){
            rs += ", cost=" + properties.encode();
        }
        return rs+'}';
    }
}
