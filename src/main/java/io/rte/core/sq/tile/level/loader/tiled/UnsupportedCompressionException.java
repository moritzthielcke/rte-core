package io.rte.core.sq.tile.level.loader.tiled;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description:
 */
public class UnsupportedCompressionException extends Exception{
    public UnsupportedCompressionException(String msg) {
        super(msg);
    }
}
