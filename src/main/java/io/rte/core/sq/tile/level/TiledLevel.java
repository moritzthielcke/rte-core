package io.rte.core.sq.tile.level;

import io.rte.core.sq.tile.Tile;
import io.rte.core.sq.tile.TileType;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description:
 */
public class TiledLevel {
    private static final Logger log = LoggerFactory.getLogger(TiledLevel.class);
    private final String name;
    private final int height;
    private final int width;
    private final List<JsonObject> inputLayer;
    private final Map<Integer, TileType> tileTypes = new HashMap<>(); //maps the id(in the level) of the tile to its type
    private Map<Integer, TiledLayer> layer = new HashMap<>();
    private TiledLayer aggregationLayer;


    public TiledLevel(String name, int height, int width, List<JsonObject> inputLayer) {
        this.name = name;
        this.height = height;
        this.width = width;
        this.inputLayer = inputLayer;
    }


    /***
     *
     * @param target aggregate target
     * @param from aggregate from
     */
    public void aggregate(Tile target, Tile from){
        if(!from.isWalkAble()){
            target.setWalkAble(false);
        }
        if(!from.isAccessAble()){
            target.setAccessAble(false);
        }
        target.setCost(target.getCost() + from.getCost());
    }


    public void addTileType(int id, TileType type){
        tileTypes.put(id, type);
    }

    public TiledLayer getAggregationLayer() {
        return aggregationLayer;
    }

    public void setAggregationLayer(TiledLayer aggregationLayer) {
        this.aggregationLayer = aggregationLayer;
    }

    public Map<Integer, TiledLayer> getLayer() {
        return layer;
    }

    public Map<Integer, TileType> getTileTypes() {
        return tileTypes;
    }

    public List<JsonObject> getInputLayer() {
        return inputLayer;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Level{" +
                "name='" + name + '\'' +
                ", ref='" + super.toString() + '\'' +
                ", height=" + height +
                ", width=" + width +
                '}';
    }
}
