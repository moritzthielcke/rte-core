package io.rte.core.sq.tile.pathfinder;

import io.rte.core.algo.astar.Graph;
import io.rte.core.algo.astar.Pathfinder;
import io.rte.core.sq.tile.Tile;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description: Builds a path based on the TileScorer
 * @see TileScorer
 */
public class TilePathfinder extends Pathfinder<Tile> {
    public TilePathfinder(Graph<Tile> graph) {
        super(graph, new TileScorer(), new TileScorer());
    }
    public TilePathfinder(TileNavigation navigation){
        this(navigation.getGraph());
    }
}
