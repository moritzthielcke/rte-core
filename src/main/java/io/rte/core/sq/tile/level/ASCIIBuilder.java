package io.rte.core.sq.tile.level;

import io.rte.core.sq.tile.Tile;
import io.rte.core.sq.tile.TileSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description:
 */
public class ASCIIBuilder {
    public enum Mode{
        MAP, POSITION, COST
    }
    private TileSet tileset;

    public ASCIIBuilder(TileSet tileset) {
        this.tileset = tileset;
    }

    public List<String> renderRows() {
        return renderRows( Mode.MAP );
    }

    public List<String> renderRows(Mode mode){
        List<String> result = new ArrayList<>();
        //we just assume every row has the same length
        int rows = tileset.getTiles()[0].length;
        for(int y = 0; y < rows; y++){
            StringBuilder builder = new StringBuilder();
            for(int x=0; x<tileset.getTiles().length; x++){
                Tile t = tileset.getTile(x,y);
                if(mode.equals(Mode.POSITION)){
                    builder.append("[").append(t.getX()).append("|").append(t.getY()).append("]");
                }
                else if (mode.equals(Mode.MAP) || mode.equals(Mode.COST)){
                    String cost = mode.equals(Mode.COST) ? String.valueOf(t.getCost()) : "o";
                    String symbol = t.isWalkAble() ? cost : "x";
                    builder.append("[").append(symbol).append("]");
                }
            }
            result.add(builder.toString());
        }
        return result;
    }


}
