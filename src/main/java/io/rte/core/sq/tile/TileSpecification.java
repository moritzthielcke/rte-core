package io.rte.core.sq.tile;

import io.vertx.core.json.JsonObject;

/**
 * Author:      moritz thielcke
 * Date:        05.01.2021
 * Description: a tile specification
 * * todo add object toM on-Methods
 */
public class TileSpecification {
    private String id;
    private JsonObject properties = new JsonObject(); //default properties

    public TileSpecification(String tileId){
        this.id = tileId;
    }

    public String getId() {
        return id;
    }

    public JsonObject getProperties() {
        return properties;
    }

    public void onCreate(Tile t){}

    /***
     * some steps on the tile
     */
    public void onStep(Tile t){ }

}
