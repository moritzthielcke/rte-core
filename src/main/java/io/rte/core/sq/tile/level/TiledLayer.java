package io.rte.core.sq.tile.level;

import io.rte.core.sq.tile.TileSet;

/**
 * Author:      moritz thielcke
 * Date:        04.01.2021
 * Description:
 */
public class TiledLayer {
    private Integer id;
    private String name;
    private String type;
    private TileSet tileSet;

    public TiledLayer(Integer id, String name, String type, TileSet tileSet) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.tileSet = tileSet;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public TileSet getTileSet() {
        return tileSet;
    }
}
