package io.rte.core.sq.tile;

import io.vertx.core.json.JsonObject;

/**
 * Author:      moritz thielcke
 * Date:        05.01.2021
 * Description:
 */
public class TileType {
    private String source;
    private Integer sourceId;
    private Integer id;
    //properties
    private boolean walkAble;
    private int cost;
    private JsonObject properties;
    private TileSpecification specification;

    public TileType(Integer id, String source, Integer sourceId, boolean walkAble, int cost, JsonObject properties) {
        this.source = source;
        this.sourceId = sourceId;
        this.walkAble = walkAble;
        this.cost = cost;
        this.properties = properties;
        this.id = id;
    }


    public Integer getId() {
        return id;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public String getSource() {
        return source;
    }

    public boolean isWalkAble() {
        return walkAble;
    }

    public int getCost() {
        return cost;
    }

    public JsonObject getProperties() {
        return properties;
    }

    public TileSpecification getSpecification() {
        return specification;
    }

    public void setSpecification(TileSpecification specification) {
        this.specification = specification;
    }

    @Override
    public String toString() {
        return "TileType{" +
                "sourceFile='" + source + '\'' +
                ", sourceId=" + sourceId +
                ", id=" + id +
                ", walkAble=" + walkAble +
                ", cost=" + cost +
                ", spec=" + specification +
                ", props = "+properties.encode()+
                '}';
    }
}
