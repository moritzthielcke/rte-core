package io.rte.core.sq.tile;

import io.rte.core.sq.tile.level.TiledLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Author:      moritz thielcke
 * Date:        05.01.2021
 * Description: a basic tile game
 */
public abstract class AbstractTileGame {
    private static final Logger log = LoggerFactory.getLogger(AbstractTileGame.class);
    private final List<TiledLevel> level = new ArrayList<>();
    private final String name;

    public AbstractTileGame(String name) {
        this.name = name;
    }

    public abstract Map<String, TileSpecification> getTileSpecification();

    public List<TiledLevel> getLevel() {
        return level;
    }

    //tile type impls?

    //defines aggretion? (to from tile to tile, optional based on layertype)

    //defines scorer
    //defines Tile-Prototypes
    


}
