package io.rte.core.algo.astar;

/**
 * Author:      moritz thielcke
 * Date:        02.01.2021
 * Description: scores the cost for traveling from one node to another
 */
public interface Scorer<T extends GraphNode> {
    double computeCost(T from, T to);
}