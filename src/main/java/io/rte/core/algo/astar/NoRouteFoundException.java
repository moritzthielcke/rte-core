package io.rte.core.algo.astar;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description:
 */
public class NoRouteFoundException extends Exception{
    public NoRouteFoundException(String msg){
        super(msg);
    }
}
