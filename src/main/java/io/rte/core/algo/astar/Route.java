package io.rte.core.algo.astar;

/**
 *
 * Author:      moritz thielcke
 * Date:        02.01.2021
 * Description: represents a route for the pathfinding algo
 */
public class Route <T extends GraphNode> implements Comparable<Route>{
    private final T current;
    private T previous;
    private double routeScore;
    private double estimatedScore;


    /***
     *
     * @param current
     * @param routeScore
     * @param estimatedScore
     * @param previous
     */
    Route(T current, double routeScore, double estimatedScore,  T previous) {
        this.current = current;
        this.previous = previous;
        this.routeScore = routeScore;
        this.estimatedScore = estimatedScore;
    }

    Route(T current) {
        this(current, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, null);
    }

    Route(T current, double routeScore, double estimatedScore) {
        this(current, routeScore, estimatedScore, null);
    }

    public int compareTo(Route other) {
        if (this.estimatedScore > other.estimatedScore) {
            return 1;
        } else if (this.estimatedScore < other.estimatedScore) {
            return -1;
        } else {
            return 0;
        }
    }

    public T getCurrent() {
        return current;
    }

    public T getPrevious() {
        return previous;
    }

    public void setPrevious(T previous) {
        this.previous = previous;
    }

    public double getRouteScore() {
        return routeScore;
    }

    public void setRouteScore(double routeScore) {
        this.routeScore = routeScore;
    }

    public double getEstimatedScore() {
        return estimatedScore;
    }

    public void setEstimatedScore(double estimatedScore) {
        this.estimatedScore = estimatedScore;
    }

    @Override
    public String toString() {
        return "Route{" +
                "current=" + current +
                ", previous=" + previous +
                ", routeScore=" + routeScore +
                ", estimatedScore=" + estimatedScore +
                '}';
    }
}
