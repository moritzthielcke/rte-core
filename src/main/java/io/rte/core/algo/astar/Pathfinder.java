package io.rte.core.algo.astar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Author:      moritz thielcke
 * Date:        02.01.2021
 * Description: A* Pathfinder
 */
public class Pathfinder <T extends GraphNode> {
    private static final Logger log = LoggerFactory.getLogger(Pathfinder.class);
    private final Graph<T> graph;
    private final Scorer<T> nextNodeScorer;
    private final Scorer<T> targetScorer;

    public Pathfinder(Graph<T> graph, Scorer<T> nextNodeScorer, Scorer<T> targetScorer) {
        this.graph = graph;
        this.nextNodeScorer = nextNodeScorer;
        this.targetScorer = targetScorer;
    }

    /***
     * finds a route
     * @param from start position
     * @param to target position
     * @return
     */
    public List<T> findRoute(T from, T to) throws NoRouteFoundException {
        Map<T, Route<T>> allNodes = new HashMap<>();
        Queue<Route> openSet = new PriorityQueue<>();
        Route<T> start = new Route<T>(from, 0d, targetScorer.computeCost(from, to), null);
        allNodes.put(from, start);
        openSet.add(start);
        while (!openSet.isEmpty()) {
            Route<T> next = openSet.poll();
            log.debug("open set of {} ", openSet.stream().map(Route::getCurrent).collect(Collectors.toSet()));
            log.debug("looking at node: " + next);
            if (next.getCurrent().equals(to)) {
                log.debug("found our destination !");
                List<T> route = new ArrayList<>();
                Route<T> current = next;
                do {
                    route.add(0, current.getCurrent());
                    current = allNodes.get(current.getPrevious());
                } while (current != null);
                return route;
            }
            graph.getConnections(next.getCurrent()).forEach(connection -> {
                double newScore = next.getRouteScore() + nextNodeScorer.computeCost(next.getCurrent(), connection);
                Route<T> nextNode = allNodes.getOrDefault(connection, new Route<T>(connection));
                allNodes.put(connection, nextNode);
                if (nextNode.getRouteScore() > newScore) {
                    nextNode.setPrevious(next.getCurrent());
                    nextNode.setRouteScore(newScore);
                    nextNode.setEstimatedScore(newScore + targetScorer.computeCost(connection, to));
                    openSet.add(nextNode);
                    log.debug("Found a better route to node: {} ", nextNode);
                }
            });

        }
        throw new NoRouteFoundException("No route found");
    }

}
