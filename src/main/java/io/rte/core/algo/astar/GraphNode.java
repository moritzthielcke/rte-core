package io.rte.core.algo.astar;

/**
 * Author:      moritz thielcke
 * Date:        02.01.2021
 * Description: a node in the graph
 */
public interface GraphNode {
    String getId();
}
