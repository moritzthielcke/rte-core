package io.rte.core.algo.astar;

import java.util.Map;
import java.util.Set;

/**
 *
 * Author:      moritz thielcke
 * Date:        02.01.2021
 * Description: a graph is a set of nodes with all existing connections between them
 */
public class Graph<T extends GraphNode> {
    private final Set<T> nodes;
    private final Map<String, Set<T>> connections;

    /**
     *
     * @param nodes
     * @param connections
     */
    public Graph(Set<T> nodes, Map<String, Set<T>> connections) {
        this.nodes = nodes;
        this.connections = connections;
    }

    /***
     * returns all the nodes in the graph
     * @return
     */
    public Set<T> getNodes() {
        return nodes;
    }

    /***
     * returns the connections for a given node in the graph
     * @param node
     * @return
     */
    public Set<T> getConnections(T node) {
        return this.connections.get(node.getId());
    }
}
