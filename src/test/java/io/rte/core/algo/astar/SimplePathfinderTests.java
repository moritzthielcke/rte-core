package io.rte.core.algo.astar;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * Author:      moritz thielcke
 * Date:        02.01.2021
 * Description:
 */
class SimplePathfinderTests {
    private static final Logger log = LoggerFactory.getLogger(SimplePathfinderTests.class);
    private static final int NODECOUNT = 1000;
    private static final List<SimpleNode> nodes = new ArrayList<>(NODECOUNT);

    @BeforeAll
    static void setUp() {
        IntStream.rangeClosed(0,NODECOUNT-1).forEach(i -> nodes.add(new SimpleNode(i)));
    }

    @Test
    void findRoute() throws NoRouteFoundException {
        //build connections
        Map<String, Set<SimpleNode>> map = new HashMap<>();
        nodes.forEach( simpleNode -> {
            Set<SimpleNode> connections = new HashSet<>();
            IntStream.of(1,2,3,5,8).forEach( i -> {
                int index = simpleNode.getNum()+i;
                //only connect even nodes:
                if(index < NODECOUNT && index % 2 == 0){
                    SimpleNode waypoint = nodes.get(index);
                    if(waypoint.getNum() != 120){ //to show of we are actually finding something ;)
                        connections.add(waypoint);
                    }
                }
            });
            map.put(simpleNode.getId(), connections);
        });
        //create graph and pathfinder
        Graph<SimpleNode> graph = new Graph<>(new HashSet<>(nodes),map);
        Pathfinder<SimpleNode> pf = new Pathfinder<>(graph, new SimpleScorer(), new SimpleScorer());
        List<SimpleNode> route = pf.findRoute(nodes.get(0), nodes.get(888));
        log.info("Result Path = {}", route.stream().map(SimpleNode::getId).collect(Collectors.toList()));
        System.out.println();
    }
}