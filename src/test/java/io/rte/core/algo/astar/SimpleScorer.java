package io.rte.core.algo.astar;

/**
 * Author:      moritz thielcke
 * Date:        02.01.2021
 * Description: Simplescorer uses the abs of ids to calculate distances
 */
public class SimpleScorer implements Scorer<SimpleNode>{
    @Override
    public double computeCost(SimpleNode from, SimpleNode to) {
        return  Math.abs(to.getNum() - from.getNum());
    }
}
