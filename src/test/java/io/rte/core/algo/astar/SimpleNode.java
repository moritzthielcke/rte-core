package io.rte.core.algo.astar;

/**
 * Author:      moritz thielcke
 * Date:        02.01.2021
 * Description:
 */
public class SimpleNode implements GraphNode{
    private String id;
    private int num;

    public SimpleNode(int num) {
        this.num = num;
        this.id = String.valueOf(num);
    }

    public int getNum() {
        return num;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "SimpleNode{" +
                "id='" + id +"'}";
    }
}
