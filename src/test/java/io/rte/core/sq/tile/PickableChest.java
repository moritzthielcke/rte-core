package io.rte.core.sq.tile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Author:      moritz thielcke
 * Date:        06.01.2021
 * Description:
 */
public class PickableChest extends TileSpecification{
    private static final Logger log = LoggerFactory.getLogger(PickableChest.class);

    public PickableChest(String tileId) {
        super(tileId);
        getProperties().put("pickable", true);
    }

    @Override
    public void onCreate(Tile t) {
        log.info("pickable chest is on tile -> {}|{}", t.getX(), t.getY());
    }

    @Override
    public void onStep(Tile t) {
        t.getProperties().put("pickable", false);
        log.info("i picked up a chest!");
    }
}
