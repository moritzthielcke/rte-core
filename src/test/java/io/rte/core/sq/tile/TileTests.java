package io.rte.core.sq.tile;

import io.rte.core.algo.astar.NoRouteFoundException;
import io.rte.core.algo.astar.Pathfinder;
import io.rte.core.sq.tile.level.ASCIIBuilder;
import io.rte.core.sq.tile.pathfinder.TileNavigation;
import io.rte.core.sq.tile.pathfinder.TilePathfinder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description: Testcoverage for Tiles
 * @link io.rte.core.tile
 */
class TileTests {
    private static final Logger log = LoggerFactory.getLogger(TileTests.class);
    private static TileSet<Tile> tileset;

    @BeforeAll
    public static void setUp(){
        tileset = RandomLevelGenerator.generateRandomCostMap();
    }

    @Test
    void testTileNavigation() throws NoRouteFoundException {
        Pathfinder<Tile> pf = new TilePathfinder( new TileNavigation<>(tileset) );
        List<Tile> route = pf.findRoute(tileset.getTile(0,3), tileset.getTile(79,40));
        log.info("Result of A* pathfinder -> Steps: {}, totalCost = {}, route = {}",
                route.size(),
                route.stream().map(Tile::getCost).reduce(0, Integer::sum),
                route.stream().map(Tile::toString).collect(Collectors.joining(" -> "))
        );
    }

    @Test
    void testMap() {
        Assertions.assertEquals(98, tileset.getTile(98,44).getX());
        Assertions.assertEquals(0, tileset.getTile(0,0).getX());
        log.info("test tile : {}", tileset.getTile(0, 44));
    }

    @Test
    void testEchoCostMap() {
        //echo what we have created:
        ASCIIBuilder buildMap = new ASCIIBuilder(tileset);
        log.info("tileset : {} ", tileset);
        log.info("Position-Map:");
        buildMap.renderRows(ASCIIBuilder.Mode.POSITION).forEach(log::info);
    }

}