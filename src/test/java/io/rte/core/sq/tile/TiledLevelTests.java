package io.rte.core.sq.tile;

import io.rte.core.algo.astar.NoRouteFoundException;
import io.rte.core.algo.astar.Pathfinder;
import io.rte.core.sq.tile.level.ASCIIBuilder;
import io.rte.core.sq.tile.level.TiledLayer;
import io.rte.core.sq.tile.level.TiledLevel;
import io.rte.core.sq.tile.level.loader.tiled.TiledMapLoader;
import io.rte.core.sq.tile.pathfinder.TileNavigation;
import io.rte.core.sq.tile.pathfinder.TilePathfinder;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description:
 */
@ExtendWith(VertxExtension.class)
class TiledLevelTests {
    private static final Logger log = LoggerFactory.getLogger(TiledLevelTests.class);
    private static final String ASSET_DIR="./games/prototype/maps";
    private static Vertx vertx;
    private AbstractTileGame game;

    @BeforeAll
    public static void setup() {
        vertx = Vertx.vertx();
    }

    @BeforeEach
    public void setupTest(){
        game = new AbstractTileGame("prototype") {
            @Override
            public Map<String, TileSpecification> getTileSpecification() {
                Map<String, TileSpecification> specs = new HashMap<>();
                PickableChest magicChest = new PickableChest("buch-outdoor.json:10");
                specs.put(magicChest.getId(), magicChest);
                return specs;
            }
        };
    }


    @Test
    public void testPathFinder() throws Throwable {
        VertxTestContext testContext = new VertxTestContext();

        TiledMapLoader loader = new TiledMapLoader(ASSET_DIR, vertx.fileSystem(), game);
        Future<TiledLevel> loadMap = loader.loadMapFromJsonFile( "desert", "desert.map.json").future();
        loadMap.onFailure(testContext::failNow);
        loadMap.onSuccess(level -> {
            TileSet tiles = level.getAggregationLayer().getTileSet();
            Pathfinder<Tile> pf = new TilePathfinder(new TileNavigation<>(tiles));
            List<Tile> route = null;
            try {
                route = pf.findRoute(tiles.getTile(0,3), tiles.getTile(94,45)); //our pickable chest is here
            } catch (NoRouteFoundException e) {
                testContext.failNow(e);
            }
            log.info("Result of A* pathfinder -> Steps: {}, totalCost = {}, route = {}",
                    route.size(),
                    route.stream().map(Tile::getCost).reduce(0, Integer::sum),
                    route.stream().map(Tile::toString).collect(Collectors.joining(" -> "))
            );
            testContext.completeNow();
        });

        assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
        if (testContext.failed()) {
            throw testContext.causeOfFailure();
        }
    }

    @Test
    public void testReactions() throws Throwable{
        //todo not a real test method, just example code for going thru layers (eg for real player movement)
        VertxTestContext testContext = new VertxTestContext();

        TiledMapLoader loader = new TiledMapLoader(ASSET_DIR, vertx.fileSystem(), game);
        Future<TiledLevel> loadMap = loader.loadMapFromJsonFile( "desert", "desert.map.json").future();
        loadMap.onFailure(testContext::failNow);
        loadMap.onSuccess(level -> {
            level.getLayer().forEach((integer, tiledLayer) -> {
                if("Objects".equals(tiledLayer.getName())){
                    Tile magicChest = tiledLayer.getTileSet().getTile(94,45);
                    magicChest.getType().getSpecification().onStep(magicChest);
                }
            });
            testContext.completeNow();
        });

        assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
        if (testContext.failed()) {
            throw testContext.causeOfFailure();
        }
    }


    @Test
    public void testTiledMapLoader() throws Throwable {
        VertxTestContext testContext = new VertxTestContext();
        TiledMapLoader loader = new TiledMapLoader(ASSET_DIR, vertx.fileSystem(), game);
        Future<TiledLevel> loadMap = loader.loadMapFromJsonFile( "desert", "desert.map.json").future();
        loadMap.onFailure(testContext::failNow);
        loadMap.onSuccess(level -> {
            log.info("Desert Map: {}", level);
            level.getInputLayer().forEach(l -> {
                log.info(" Input-Layer : {}", l);
            });
            level.getLayer().forEach((name, layer) -> {
                ASCIIBuilder ascii = new ASCIIBuilder(layer.getTileSet());
                log.info(" tileset {} : {} ", name, layer.getTileSet());
                log.info("  Cost-Map:");
                ascii.renderRows(ASCIIBuilder.Mode.COST).forEach(log::info);
            });
            //show aggregation layer:
            TiledLayer aggregationLayer = level.getAggregationLayer();
            ASCIIBuilder ascii = new ASCIIBuilder(aggregationLayer.getTileSet());
            log.info("aggregation layer: {}", aggregationLayer);
            log.info("  Layer-Positions:");
            ascii.renderRows(ASCIIBuilder.Mode.POSITION).forEach(log::info);
            log.info("  Cost-Map:");
            ascii.renderRows(ASCIIBuilder.Mode.COST).forEach(log::info);
            try {
                assertEquals(30, level.getLayer().get(1).getTileSet().getTile(10, 30).getY());
            }catch(Exception ex){
                testContext.failNow(ex);
            }
            testContext.completeNow();
        });

        assertTrue(testContext.awaitCompletion(5, TimeUnit.SECONDS));
        if (testContext.failed()) {
            throw testContext.causeOfFailure();
        }
    }

}