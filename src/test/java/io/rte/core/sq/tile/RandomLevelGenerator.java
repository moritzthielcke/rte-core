package io.rte.core.sq.tile;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Author:      moritz thielcke
 * Date:        03.01.2021
 * Description:
 */
public class RandomLevelGenerator {


    /**
     * will generate a map with random travel cost
     *
     * @return
     */
    public static TileSet<Tile> generateRandomCostMap(){
        //build demo level
        int sizeX = 100;
        int sizeY = 55;
        List<List<Tile>> tiles = new ArrayList<>(sizeX);
        for(int x = 0; x < sizeX; x++){
            tiles.add(new ArrayList<>(sizeY));
            for(int y = 0; y < sizeY; y++){
                tiles.get(x).add(new Tile(x,y));
            }
        }
        //make random some stuff not walkable
        new Random().ints(50, 5, 99).forEach(x ->
                new Random().ints(25, 0, 45).forEach(
                        y -> tiles.get(x).get(y).setWalkAble(false))
        );
        //make a little wall
        List<Tile> row = tiles.get(2);
        for(int i = 0; i < row.size(); i++){
            if(i < 45 || i > 50){
                row.get(i).setWalkAble(false);
            }
        }
        //make stuff hard to walk
        Random costSeed = new Random();
        new Random().ints(99, 0, 99).forEach( x ->
                new Random().ints(54, 0, 54).forEach(
                        y -> tiles.get(x).get(y).setCost( costSeed.nextInt(100) )
                )
        );
        //persist to array and save to a tileset
        Tile[][] array = new Tile[tiles.size()][];
        for(int i=0; i < tiles.size(); i++) {
            array[i] = tiles.get(i).toArray(new Tile[0]);
        }
        return new TileSet<Tile>(array);
    }

}
